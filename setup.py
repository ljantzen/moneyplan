from setuptools import setup

setup(
    name='moneyplan',
    version='0.9',
    packages=['mynab', 'sbanken', 'moneyplan'],
    url='https://github..com/ljantzen/moneyplan',
    license='MIT',
    author='leifj',
    author_email='ljantzen@gmail.com',
    description=''
)
