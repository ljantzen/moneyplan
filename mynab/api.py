""" API for accessing YNAB """
import datetime
import hashlib
import logging
from typing import Dict, Any, List

import ynab
from ynab import SaveTransactionsWrapper, SaveTransaction

import mynab.config
import mynab.models
import sbanken.models
from mynab.models import YnabAccount, YnabTransaction, YnabCategory

logger = logging.getLogger("mynab.api")

try:
    configuration = ynab.Configuration()
    configuration.api_key['Authorization'] = mynab.config.YNAB_API_KEY
    configuration.api_key_prefix['Authorization'] = 'Bearer'
except KeyError as e:
    logger.error("Ugyldig oppsett i ynab pålogging: " + str(e))


def fetch_accounts():
    logger.debug("Henter kontoer fra ynab")
    accounts_api = ynab.AccountsApi()
    accounts = accounts_api.get_accounts(mynab.config.YNAB_BUDGET_ID)
    result: Dict[Any, YnabAccount] = {}

    for a in accounts.data.accounts:
        account = mynab.models.YnabAccount(a.name, a.balance, a.id, a.transfer_payee_id)
        result[a.name] = account

    logger.debug(str(result))
    return result


def fetch_categories() -> List[YnabCategory]:
    logger.debug("Henter kategorier fra ynab")

    categories_api = ynab.CategoriesApi()

    categories = categories_api.get_categories(mynab.config.YNAB_BUDGET_ID)
    result: List[YnabCategory] = []

    for g in categories.data.category_groups:
        if hasattr(g, 'categories'):
            for c in g.categories:
                category = mynab.models.YnabCategory(c.id, c.name, g.id, g.name)
                result.append(category)

    logger.debug("Fant " + str(len(result)) + " kategorier")

    return result


def fetch_transactions(account_id: str, since_date: str = None) -> List[YnabTransaction]:
    logger.debug("Henter transaksjoner fra ynab for konto " + account_id + " fra dato " + str(since_date))

    transactions_api = ynab.TransactionsApi()

    if since_date is None:
        since_date = str(datetime.datetime.now().date() - datetime.timedelta(7))

    transactions = transactions_api.get_transactions_by_account(mynab.config.YNAB_BUDGET_ID, account_id,
                                                                since_date=since_date)
    result: List[YnabTransaction] = []

    for t in transactions.data.transactions:
        trans = mynab.models.YnabTransaction(t.amount,
                                             t.account_id,
                                             t.memo,
                                             t.import_id,
                                             t.payee_id,
                                             t.category_id,
                                             t.approved,
                                             t.date,
                                             t.cleared)
        result.append(trans)

    logger.debug("Fant " + str(len(transactions.data.transactions)) + " transaksjoner ")

    return result


def create_transactions(sbtransactions: List[sbanken.models.SBTransaction], config, transaction_categories,
                        ynab_account_id=None):
    logger.debug("oppretter " + str(len(sbtransactions)) + " transaksjoner i ynab ")

    transactions_api = ynab.TransactionsApi()
    trans_wrapper: SaveTransactionsWrapper = ynab.SaveTransactionsWrapper()
    trans_wrapper.transaction = None
    trans_wrapper.transactions = []

    for sbtrans in sbtransactions:

        # dont do transfers here, they will be done later
        if sbtrans.kode == 200:
            continue

        t = ynab.SaveTransaction()

        if ynab_account_id is None:
            t.account_id = sbtrans.ynab_konto_id
        else:
            t.account_id = ynab_account_id

        t.amount = int(sbtrans.belop * 1000)
        t.approved = False
        t.cleared = config['cleared']
        t.flag_color = config['flagg_farge']
        t.date = sbtrans.transaksjonsdato
        t.memo = sbtrans.tekst2
        t.import_id = ("SB:" + str(t.amount) + ":" + str(t.date) + ":" + t.memo)[:36]

        if sbtrans.tekst2 in transaction_categories:
            t.payee_id = transaction_categories[sbtrans.tekst2][0]['category_payee']['payee']
            t.category_id = transaction_categories[sbtrans.tekst2][0]['category_payee']['category']

        trans_wrapper.transactions.append(t)

    if len(trans_wrapper.transactions) > 0:
        transactions_api.create_transaction(mynab.config.YNAB_BUDGET_ID, trans_wrapper)


def create_unmatched_transfer_transactions(sbtransactions: List[sbanken.models.SBTransaction],
                                           config,
                                           ynab_account_id=None):
    logger.debug("oppretter " + str(len(sbtransactions)) + " transaksjoner i ynab ")

    transactions_api = ynab.TransactionsApi()
    trans_wrapper: SaveTransactionsWrapper = ynab.SaveTransactionsWrapper()
    trans_wrapper.transaction = None
    trans_wrapper.transactions = []

    for sbtrans in sbtransactions:

        t = ynab.SaveTransaction()

        if ynab_account_id is None:
            t.account_id = sbtrans.ynab_konto_id
        else:
            t.account_id = ynab_account_id

        t.amount = int(sbtrans.belop * 1000)
        t.approved = False
        t.cleared = config['cleared']
        t.flag_color = config['flagg_farge']
        t.date = sbtrans.transaksjonsdato
        t.memo = sbtrans.tekst2
        t.import_id = ("SB:" + str(t.amount) + ":" + str(t.date) + ":" + t.memo)[:36]

        trans_wrapper.transactions.append(t)

    if len(trans_wrapper.transactions) > 0:
        transactions_api.create_transaction(mynab.config.YNAB_BUDGET_ID, trans_wrapper)


def create_transfer_transaction(transfers, config):
    logger.debug("Oppretter transaksjoner i ynab for overføringer mellom kontoer")

    transactions_api = ynab.TransactionsApi()
    trans_wrapper: SaveTransactionsWrapper = ynab.SaveTransactionsWrapper()
    trans_wrapper.transaction = None
    trans_wrapper.transactions = []

    for sbtransfer in transfers:
        t: SaveTransaction = ynab.SaveTransaction()

        if sbtransfer.ynab_konto_id is not None:
            t.account_id = sbtransfer.ynab_konto_id
        else:
            t.account_id = sbtransfer.account_id

        if sbtransfer.ynab_motkonto_id is not None:
            t.payee_id = sbtransfer.ynab_motkonto_id

        t.amount = int(sbtransfer.belop * 1000)
        t.approved = False
        t.cleared = config['cleared']
        t.flag_color = config['flagg_farge']
        t.date = sbtransfer.transaksjonsdato
        t.memo = sbtransfer.tekst2
        m = hashlib.md5()
        import_id = "SB:" + str(t.amount) + ":" + str(t.date) + ":" + t.memo
        m.update(import_id.encode("utf-8"))
        t.import_id = m.hexdigest()

        trans_wrapper.transactions.append(t)

    if len(trans_wrapper.transactions) > 0:
        logger.debug("Oppretter " + str(len(trans_wrapper.transactions)) + " overføringstransaksjoner ")
        transactions_api.create_transaction(mynab.config.YNAB_BUDGET_ID, trans_wrapper)
