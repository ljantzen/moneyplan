import os

try:
    YNAB_API_KEY = os.environ['YNAB_API_KEY']

    # keep in mind that the ynab budget id changes when you do a "fresh start"
    YNAB_BUDGET_ID = os.environ['YNAB_BUDGET_ID']

    if YNAB_API_KEY is None or YNAB_BUDGET_ID is None:
        raise EnvironmentError("YNAB environment not configured")
except:
    pass

