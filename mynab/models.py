class YnabAccount :

    kontonavn: str
    balanse: int
    kontoid: str
    motkontoid: str

    def __init__(self,kontonavn,balanse,kontoid,motkontoid):
        self.kontonavn = kontonavn
        self.balanse = balanse
        self.kontoid = kontoid
        self.motkontoid = motkontoid


class YnabCategory:

    def __init__(self, category_id,category_name, group_id, group_name):
        self.category_id = category_id
        self.name = category_name
        self.group_id = group_id
        self.group_name = group_name


class YnabTransaction:

    def __init__(self, milliunits, account_id, memo, import_id, payee_id, category_id, approved, transdate, cleared):
        self.milliunits = milliunits
        self.account_id = account_id
        self.memo = memo
        self.import_id = import_id
        self.payee_id = payee_id
        self.category_id = category_id
        self.approved = approved
        self.transdate = transdate
        self.cleared = cleared

        @property
        def amount(self):
            return milliunits / 1000

        @amount.setter
        def amount(self, amount):
            self.milliunits = amount * 1000