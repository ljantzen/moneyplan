class InvalidSessionError(Exception):
    pass

class MoneyplanException(Exception):
    pass
