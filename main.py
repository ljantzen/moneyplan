""" Main program for moneyplan """

import argparse
import datetime
import json
import logging
from argparse import ArgumentParser
from typing import Dict, List, AnyStr, Any

import mynab.api
import sbanken.api

from moneyplan.exceptions import MoneyplanException
from mynab.models import YnabAccount
from sbanken.credentials import SB_CUSTOMERID
from sbanken.models import SBAccount, SBTransaction


def main():
    """
    Main for moneyplan
    Returns: Ingenting
    """

    try:

        cli_options: object = parse_command_line_options()

        configure_logging(cli_options)
        logger = logging.getLogger("moneyplan")

        config = read_config(cli_options.config)

        sbanken.api.login(config)
        sb_konto_info: Dict[AnyStr, SBAccount] = sbanken.api.fetch_accounts(SB_CUSTOMERID)

        # fetch list of ynab accounts from ynab
        ynab_konto_info: Dict[AnyStr, YnabAccount] = mynab.api.fetch_accounts()

        validate_accounts(config['kontoer_som_skal_synkroniseres'], sb_konto_info, ynab_konto_info)

        from_date = find_startdate(config, cli_options)
        ynab_date = datetime.datetime.now() - datetime.timedelta(90)

        transaction_categories = build_transaction_map(config, ynab_date, ynab_konto_info)

        sync_transactions(from_date,
                          config,
                          logger,
                          cli_options,
                          sb_konto_info,
                          ynab_konto_info,
                          transaction_categories)

        logger.info('Logger ut av Sbanken')
        sbanken.api.logout()

    except MoneyplanException as e:
        logger = logging.getLogger("moneyplan")
        logger.error("Dette gikk ikke så bra; " + str(e))


def build_transaction_map(config, from_date, ynab_konto_info):
    result = {}

    ynab_categories = mynab.api.fetch_categories()

    for konto in config['kontoer_som_skal_synkroniseres']:

        ynab_transactions = mynab.api.fetch_transactions(ynab_konto_info[konto['kontonavn']].kontoid, from_date)

        for t in ynab_transactions:
            if t.memo not in result:
                result[t.memo] = [{'count': 1, 'category_payee': {'category': t.category_id, 'payee': t.payee_id}}]
            else:
                found = False
                data = result[t.memo]
                for i, d in enumerate(data):
                    category_payee = d['category_payee']
                    if category_payee['category'] == t.category_id and category_payee['payee'] == t.payee_id:
                        found = True
                        result[t.memo][i]['count'] += 1

                if not found:
                    result[t.memo].append({'count': 1, 'category_payee': {'category': t.category_id, 'payee': t.payee_id}})

    return result


def sync_transactions(from_date, config, logger, opts, sb_konto_info, ynab_konto_info, transaction_categories):
    """

    :param from_date:
    :param config:
    :param logger:
    :param opts:
    :param sb_konto_info:
    :param ynab_konto_info:
    :return:
    """

    # hash with kontonummer as key, contains a list of transactions transfering money between own accounts in
    # SBanken needs to be handed separately as Sbanken does not tell us which account the money was transferred
    # from/to.

    transfer_transactions: Dict[str, List[SBTransaction]] = {}

    # iterate over all acounts we want to import data for
    for konto in config['kontoer_som_skal_synkroniseres']:

        kontonummer: str = konto['kontonummer']
        kontonavn: str = konto['kontonavn']

        sb_konto_id: str = sb_konto_info[kontonummer].kontoid

        logger.info("Behandler konto " + konto['kontonavn'])

        sb_transactions: List[SBTransaction] = sbanken.api.fetch_transactions(SB_CUSTOMERID,
                                                                              sb_konto_id,
                                                                              kontonummer,
                                                                              from_date)

        logger.debug('Matcher overføringstransaksjoner')
        transfer_transactions[kontonavn] = find_transfer_transactions(sb_transactions)

        if sb_transactions is not None and len(sb_transactions) > 0:
            if opts.readonly:
                logger.info("Skipper lagring av " + str(len(sb_transactions)) + " transaksjoner")
            else:
                mynab.api.create_transactions(sb_transactions,
                                              config,
                                              transaction_categories,
                                              ynab_konto_info[kontonavn].kontoid
                                              )

    # create transfer transactions if necessary
    if len(transfer_transactions) > 1:

        if opts.readonly:
            logger.info("Skipper lagring av " + str(len(transfer_transactions)) + " overføringstransaksjoner")
        else:
            logger.info('Matcher overføringstransaksjoner mot hverandre')
            transactions = match_transactions(transfer_transactions, ynab_konto_info)
            if len(transactions['matched']) > 0:
                mynab.api.create_transfer_transaction(transactions['matched'], config)
            if len(transactions['unmatched']) > 0:
                mynab.api.create_unmatched_transfer_transactions(transactions['unmatched'], config)


def find_startdate(config, opts) -> datetime:
    """
    Finn ut hvor langt tilbake i tid vi skal hente transaksjoner for

    :param config: config file settings
    :param opts:  command line options
    :return: datetime
    """
    if opts.dato is not None:
        from_date = datetime.datetime.strptime(opts.dato, '%Y%m%d')
    else:
        if opts.dager is not None:
            from_date = datetime.datetime.now().date() - datetime.timedelta(opts.dager)
        else:
            from_date = datetime.datetime.now().date() - datetime.timedelta(config['antall_dager_bakover'])
    return from_date


def find_transfer_transactions(sbtransactions: List[SBTransaction]) -> List[SBTransaction]:
    """
      Transfer transactions are marked with kode = 200.  Return a list of all
      transactions with code 200
    """

    transfers: List[SBTransaction] = []
    for sbtrans in sbtransactions:
        if sbtrans.kode == 200:
            transfers.append(sbtrans)
    return transfers


def match_transactions(transfers_per_konto, ynab_konto_info):
    """
      Scan through all transactions to find transfers from one account to another
    """

    matched_transactions = []
    unmatched_transactions = []

    for konto1 in transfers_per_konto:

        transfers_konto1 = transfers_per_konto[konto1]
        if len(transfers_konto1) == 0:
            continue

        for konto2 in transfers_per_konto:
            if konto2 == konto1:
                continue

            transfers_konto2 = transfers_per_konto[konto2]

            if len(transfers_konto2) == 0:
                continue

            for t1 in transfers_konto1:
                for t2 in transfers_konto2:
                    if (t1.transaksjonsdato == t2.transaksjonsdato
                            and abs(t1.belop) == abs(t2.belop)
                            and t1.belop + t2.belop == 0
                            and t1.matched is False
                            and t2.matched is False
                    ):
                        t1.ynab_konto_id = ynab_konto_info[konto1].kontoid
                        t1.ynab_motkonto_id = ynab_konto_info[konto2].motkontoid
                        t1.matched = True
                        t2.matched = True

                        matched_transactions.append(t1)
                        break

    for k in transfers_per_konto:
        for t in transfers_per_konto[k]:
            t.ynab_konto_id = ynab_konto_info[k].kontoid
            if t.matched:
                matched_transactions.append(t)
            else:
                unmatched_transactions.append(t)

    result: Dict[str, List[SBTransaction]] = {"matched": matched_transactions, "unmatched": unmatched_transactions}

    return result


def validate_accounts(kontoer_som_skal_synkes, sb_konto_info, ynab_konto_info):
    """
    Valider at Sbanken-kontoer finnes i Ynab og vice versa

    :param kontoer_som_skal_synkes: liste med navn/kontonr som skal synces.  hentet fra config-fil.
    :param sb_konto_info: Kontoinfo hentet fra Sbanken
    :param ynab_konto_info: Kontoinfo hentet fra Ynab
    :return:
    """

    for k in kontoer_som_skal_synkes:
        kontonavn = k['kontonavn']
        found = False

        for sbanken_konto in sb_konto_info:

            if not sbanken_konto == k['kontonummer']:
                continue

            for ynab_konto in ynab_konto_info.keys():
                if kontonavn == ynab_konto:
                    found = True
                    break

        if not found:
            raise MoneyplanException("Konto " + kontonavn + "/" + sbanken_konto +  " Ikke funnet i YNAB")


def configure_logging(opts):
    """
      configure logging based on command line switches
    """

    if opts.quiet:
        logging.basicConfig(level=logging.ERROR)
    else:
        if opts.verbose:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    requests_logger = logging.getLogger("requests_oauthlib")
    requests_logger.level = logging.ERROR


def parse_command_line_options() -> object:
    """
       parse command line swithces
    """
    parser: ArgumentParser = argparse.ArgumentParser()

    parser.add_argument("-c", "--config", default="config.json", help="sti til json konfigurasjonsfil")
    parser.add_argument("-d", "--dager", type=int, help="hent transaksjoner gitt antall dager bakover")
    parser.add_argument("-D", "--dato", help="hent transaksjoner siden angitt dato. Formatet er YYYY-MM-DD")
    parser.add_argument("-q", "--quiet", action='store_true', help="ikke skriv til terminalen når programmet kjører")
    parser.add_argument("-r", "--readonly", action='store_true', help="Ikke synk med ynab")
    parser.add_argument("-v", "--verbose", action='store_true', help="logg debug-info. Blir overstyrt av -q")
    parser.add_argument("-x", "--experimental", action='store_true', help="aktiver eksperimentelle features")

    return parser.parse_args()


def read_config(config_file):
    """
       read configuration file
    """
    try:
        with open(config_file, "r") as c:
            config = json.load(c)

    except IOError as e:
        raise MoneyplanException(e)

    return config


if __name__ == "__main__":
    """"
    main routine entry point 
    """
    main()
