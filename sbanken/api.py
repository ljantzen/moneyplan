"""
An API-wrapper for SBanken API
"""
import logging
from typing import Dict, Any, List

import sbanken.api_implementation
import sbanken.credentials
import sbanken.models
from moneyplan.exceptions import InvalidSessionError
from sbanken.models import SBAccount, SBTransaction

logger = logging.getLogger("sbanken.api")
_session = None


def login(config):
    """
    Create a session for communicating with SBanken

    Arguments:
        None
    Returns:
        Nothing
    """
    global _session

    logger.debug("Logging in to sbanken")

    sb_clientid = None
    sb_secret = None

    if 'authentication' in config:
        if 'sb_clientid' in config['authentication']:
            sb_clientid = config['authentication']['sb_clientid']
        if 'sb_secret' in config['authentication']:
            sb_secret = config['authentication']['sb_secret']

    if sbanken.credentials.SB_CLIENTID is not None:
        sb_clientid = sbanken.credentials.SB_CLIENTID

    if sbanken.credentials.SB_SECRET is not None:
        sb_secret = sbanken.credentials.SB_SECRET

    if sb_clientid is None:
        raise ValueError("Ingen gyldig verdi for SB_CLIENTID funnet. Sjekk miljøvariable eller config.json")

    if sb_secret is None:
        raise ValueError("Ingen gyldig verdi for SB_SECRET funnet. Sjekk miljøvariable eller config.json")

    if sbanken.credentials.SB_CUSTOMERID is None:
        raise ValueError("Ingen gyldig verdi for SB_CUSTOMERID funnet. Sjekk miljøvariable eller config.json")

    _session = sbanken.api_implementation.login(sb_clientid, sb_secret)

    logger.info("Logged in to SBanken successfully")


def logout():
    """
        Terminate SBanken session
    """
    global _session
    logger.debug("Logging out from SBanken ")
    if _session == None:
        raise InvalidSessionError
    _session = None
    logger.info("Logged out")


def fetch_accounts(customerid: str) -> Dict[Any, SBAccount]:
    """
    Fetch accounts for customerid
    :return:
    :param customerid: Fødselsnummer/SSN
    :return: A map of account hashes
    """

    logger.info("Henter kontoinformasjon fra Sbanken")

    global _session
    if _session == None:
        raise InvalidSessionError

    sbaccounts: object = sbanken.api_implementation.fetch_accounts(_session, customerid)

    accounts: Dict[Any, SBAccount] = {}

    a: Dict

    for a in sbaccounts:
        account = sbanken.models.SBAccount(
            a['accountNumber'],
            a['name'],
            a['balance'],
            a['accountId']
        )
        accounts[account.kontonummer] = account

    logger.info("Fant informasjon om " + str(len(accounts)) + " kontoer")

    return accounts


def fetch_transactions(customerid: str,
                       accountid: str,
                       accountnumber: str,
                       fromdate: str = None,
                       todate: str = None,
                       numItems: int = None) -> List[SBTransaction]:
    """
    Fetch a list of transactions for a customer/account combination. Limit by start/enddate and
    number of items.

    :param customerid: Fødselsnummer/SSN
    :param account:  internal SBanken account identifier
    :param fromdate:  optional.  -7 days if not specified
    :param todate:  optional. today if not specified
    :param numItems: optional. default 100.
    :return: list of sb_trans hashes

    """
    global _session
    if _session == None:
        raise InvalidSessionError

    logger.info("Henter transaksjoner for konto " + str(accountnumber))

    sbtransactions = sbanken.api_implementation.fetch_transactions(_session,
                                                                   customerid,
                                                                   accountid,
                                                                   fromdate,
                                                                   todate,
                                                                   numItems)

    transactions: List[SBTransaction] = []

    i = 0
    for trans in sbtransactions:

        if trans['source'] == "Archive":
            # source = 'Archive' betyr at transaksjonen er ferdigbehandled fra bankens side.

            sb_trans = sbanken.models.SBTransaction(
                i,
                trans['accountingDate'],
                trans['interestDate'],
                trans['amount'],
                trans['text'],
                trans['transactionTypeCode'],
                trans['transactionType'],
                accountnumber
            )

            i = i + 1

            if trans['otherAccountNumberSpecified'] == True:
                sb_trans.motkonto = trans['otherAccountNumber']

            if trans['cardDetailsSpecified'] == 'True':
                card_details_ = trans['cardDetails']
                details = sbanken.models.SBCardTransactionDetails(
                    card_details_['cardNumber'],
                    card_details_['merchantCategoryCode'],
                    card_details_['merchantName'],
                    card_details_['originalCurrencyCode'],
                    card_details_['purchaseDate'],
                    card_details_['transactionId']
                )
                sb_trans.carddetails = details

            transactions.append(sb_trans)

    logger.info("Fant " + str(len(transactions)) + " transaksjoner i sbanken for konto " + str(accountnumber))

    return transactions
