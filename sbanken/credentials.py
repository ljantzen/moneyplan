import os

SB_CUSTOMERID = None
SB_CLIENTID = None
SB_SECRET = None

try:
    SB_CUSTOMERID = os.environ['SB_CUSTOMERID']
    SB_CLIENTID = os.environ['SB_CLIENTID']
    SB_SECRET = os.environ['SB_SECRET']
except KeyError as e:
    pass


