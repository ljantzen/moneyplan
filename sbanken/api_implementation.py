"""internal implementation of data access to Sbanken"""
from oauthlib.oauth2 import BackendApplicationClient
import requests
from requests_oauthlib import OAuth2Session
import urllib.parse
import datetime

base_uri = "https://api.sbanken.no/exec.bank/api/v1/"

def enable_logging(level):
    """
    Enable transport logging

    Arguments:
        None
    Returns:
        Nothing

    """
    import logging
    import http.client

    http.client.HTTPConnection.debuglevel = 1
    logging.basicConfig()
    logging.getLogger().setLevel(level)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


def login(client_id, client_secret) -> requests.Session:
    """
    Login to sbanken using Oauth2

    Arguments:
        client_id:  Provided by SBanken
        client_secret: Provided SBanken

    Returns:
          requests.Session

    """
    oauth2_client = BackendApplicationClient(client_id=client_id)
    session = OAuth2Session(client=oauth2_client)
    secret = urllib.parse.quote(client_secret)
    session.fetch_token(
        token_url='https://auth.sbanken.no/identityserver/connect/token',
        client_id=client_id,
        client_secret=secret
    )
    return session



def fetch_accounts(session: object, customerid: str) -> object:
    """
    Fetch map of accounts for a given customerid/SSN/fødselsnummer

    Arguments:
        session: request.Session used for making http(s) requests to SBanken
        customerid: fødselsnummer

    Returns:
        a map with account number as key.  Each entry contains information for that account

    """
    uri = base_uri + "Accounts"
    response = session.get(
        uri,
        headers={'customerId': customerid}
    ).json()

    if not response["isError"]:
        return response["items"]
    else:
        raise RuntimeError("{} {}".format(response["errorType"], response["errorMessage"]))


def fetch_transactions(session : requests.Session,customerid,accountid,fromdate,todate, numItems):
    """
    Fetch transactions for a given account and optional date range

    Arguments:

        session: request.Session used for making http(s) requests to SBanken
        customerid: fødselsnummer/SSN
        accountid: Internal SBanken account id, not account number
        fromdate: If no start date is specified assume last 7 days. Otherwise string: yyyy-mm-dd
        todate: If no end date is specified, assume today. Otherwise string: yyyy-mm-dd
        numItems: Limit of number of transactions to fetch.  Max numItems=1000, default=100

    Returns:
        a list of transaction maps

    """
    if session == None:
        raise ValueError("No session provided")

    if customerid == None:
        raise ValueError("CustomerID is not specified")

    if accountid == None:
        raise ValueError("AccountId is not specified")

    if fromdate == None:
        fromdate = datetime.datetime.now().date() - datetime.timedelta(7)

    if todate == None:
        todate = datetime.datetime.now().date()

    uri =  base_uri + "Transactions/" + accountid + "?" +  "&startDate="+str(fromdate) + "&endDate=" + str(todate)

    if numItems is None:
        numItems = 1000

    if not numItems == None :
        uri = uri + "&length=" + str(numItems)

    response = session.get(
        uri,
        headers={'customerId': customerid}
    ).json()

    if not response["isError"]:
        return response["items"]
    else:
        raise RuntimeError("{} {}".format(response["errorType"], response["errorMessage"]))
