import datetime
import re
from datetime import date


class SBCardTransactionDetails:
    transid: str
    kortdato: str
    valuta: str
    kategori: str
    selger: str
    kortnummer: str

    def __init__(self, kortnummer, kategori, selger, valuta, kortdato, transid):
        self.kortnummer = kortnummer
        self.selger = selger
        self.kategori = kategori
        self.valuta = valuta
        self.kortdato = kortdato
        self.transid = transid


class SBTransaction:
    sekvensnummer: int
    regnskapsdato: str
    rentedato: str
    transaksjonsdato: str
    belop: float
    tekst: str
    tekst2: str
    kode: str
    type: str
    kontonr: str
    _carddetails: SBCardTransactionDetails
    _matched: bool
    _motkonto: str
    _ynab_konto_id: str
    _ynab_motkonto_id: str

    def __init__(self, sekvensnumer, regnskapsdato, rentedato, belop, tekst, kode, type, kontonr):
        self.sekvensnummer = sekvensnumer
        r = self.__extractdate(regnskapsdato)
        self.regnskapsdato = str(r)
        self.rentedato = str(self.__extractdate(rentedato))
        self.transaksjonsdato = str(self.__find_transactiondate(tekst, regnskapsdato))
        self.belop = belop
        self.tekst = tekst
        self.tekst2 = self.__normalize_text(tekst)
        self.kode = kode
        self.type = type
        self.kontonr = kontonr
        self._carddetails = None
        self._matched = False
        self._motkonto = None
        self._ynab_konto_id = None
        self._ynab_motkonto_id = None

    @property
    def motkonto(self):
        return self._motkonto

    @motkonto.setter
    def motkonto(self, motkonto):
        self._motkonto = motkonto

    @property
    def carddetails(self):
        return self._carddetails

    @carddetails.setter
    def carddetails(self, details):
        self._carddetails = details

    @property
    def matched(self):
        return self._matched

    @matched.setter
    def matched(self, m):
        self._matched = m

    @property
    def ynab_konto_id(self):
        return self._ynab_konto_id

    @ynab_konto_id.setter
    def ynab_konto_id(self, kontoid):
        self._ynab_konto_id = kontoid

    @property
    def ynab_motkonto_id(self):
        return self._ynab_motkonto_id

    @ynab_motkonto_id.setter
    def ynab_motkonto_id(self, kontoid):
        self._ynab_motkonto_id = kontoid

    def __str__(self):
        return self.transaksjonsdato + " " + self.tekst2.ljust(32) + " " + self.belop

    def __normalize_text(self, text: str) -> str:
        """
        Removes date, card identifier, amounts etc to create a more stable identifier that can be used
        to identify payee.

        :param text: transaction text
        :return: normalized transaction text
        """

        r1 = re.compile("^([0-9]{2})\\.([0-9]{2})")
        m = r1.match(text)

        if m is not None:  # starts with dd.dd
            s = text[6:]
        elif text.startswith("*"):
            s = text[16:]
        else:
            s = text

        r2 = re.compile("^Til:[0-9]+")
        m = r2.match(s)
        if m is not None:
            return s.upper()

        r3 = re.compile("^Til:[0-9]+")
        m = r3.match(s)
        if m is not None:
            return s.upper()

        normalized: str = re.sub(r'Til:', "", s)
        normalized = re.sub(r'Fra:', "", normalized)
        normalized = re.sub(r'Betalt:', "", normalized)
        normalized = re.sub(r'[0-9 .:]', "",
                            normalized).upper()  # remove all digits and whitespace, convert to uppercase
        if normalized.endswith("KURS"):
            normalized = normalized[:-4]

        return normalized

    def __find_transactiondate(self, text: str, defaultdate: str) -> date:
        """
        Tries to find the date the transaction was initiated.  Which is different from accountingDate and interestDate.
        This date is often encoded in the transaction text.  If not, fallback to default (accountingDate)

        :param text: transaction text
        :param defaultdate: fallback date
        :return: date of transaction
        """

        year = int(defaultdate[0:4])
        r = re.compile("^([0-9]{2})\\.([0-9]{2})")
        m = r.match(text)
        if not m is None:
            day = int(m.group(1))
            month = int(m.group(2))
        elif text.startswith("*"):
            day = int(text[6:8])
            month = int(text[9:11])
        else:
            month = int(defaultdate[5:7])
            day = int(defaultdate[8:10])

        result: date = datetime.date(year, month, day)

        return result

    def __extractdate(self, datestring):
        """
        convert YYYY-MM-DD to datetime

        :param datestring:
        :return: datetime.date
        """
        year = int(datestring[0:4])
        month = int(datestring[5:7])
        day = int(datestring[8:10])
        return datetime.date(year, month, day)


class SBAccount:
    kontoid: str
    balanse: float
    kontonavn: str
    kontonummer: str

    def __init__(self, kontonummer, kontonavn, balanse, kontoid):
        self.kontonummer = kontonummer
        self.kontonavn = kontonavn
        self.balanse = balanse
        self.kontoid = kontoid

    def __str__(self):
        return self.kontonummer + " (" + self.kontonavn + ") :" + self.balanse
