#Moneyplan

Moneyplan er et program som synkroniserer transaksjoner mellom Sbanken og YNAB.  Moneyplan er skrevet i språket Python, 
og for å kunne kjøre det må Python være installert på maskinen det kjører på.  Python kan lastes  ned fra https://python.org, og du trenger versjon 3.6 eller høyere.
Det er lurt å passe på at man installerer python i en katalogsti som ikke har noen mellomrom i navnet.  `c:\Programfiler\Python` er bedre enn `C:\Program Files\Python`. 

Det er kun transaksjonene som legges inn i YNAB.  Betalingsmottaker (payee) og kategori må settes manuelt i etterkant.  

# Autentisering

Både YNAB og SBanken benytter såkalte autentiseringstoken for å godta påloggingsforsøk.  Disse tokenene må lastes ned fra de respektive nettstedene. 

## Pålogging SBanken

For å kunne lage eller bruke programmer mot SBanken, må man inngå en utvikleravtale med SBanken.  Mer informasjon om dette på https://secure.sbanken.no/Personal/ApiBeta/Info/. 

![Sbanken avtale](docs/images/sbanken-avtale.png)

Når denne avtalen er inngått, får man muligheten til å generere et autentiseringstoken.  Dette gjøres i utviklerportalen.
![Utviklerportalen](docs/images/utviklerportalen.png).  Her må man opprette en applikasjon (i illustrasjonen kalt budgetsync), og så generere et token.

![Sbanken_apitoken](docs/images/sbanken-apitoken.png)

Det som heter Applikasjonsnøkkel i Sbanken skal inn i SB_CLIENT_ID i config.json eller i environment-variable.  Applikasjonshemmeligheten genereres ved å trykke på 
_Bestill nytt passord._  Dette passordet skal tilsvarende inn i SB_SECRET.

Disse må lagres forsvarlig, alle som har en slik vil kunne logge inn i banken på dine vegne og gjøre alt Sbanken sitt API tillater, inklusiv overføring av penger. 

## Pålogging YNAB 

YNAB har også et autentiseringstoken.  Etter innlogging går du til https://app.youneedabudget.com/settings/developer og trykker på knappen 'NewToken'

![New Token](./docs/images/ynabtoken.png)

# Installasjon av moneyplan

Hvis du er kjent med git, kan du klone dette repoet til din datamaskin.  Hvis ikke, kan du laste ned filen https://gitlab.com/ljantzen/moneyplan/-/archive/master/moneyplan-master.zip

Gå til katalogen hvor du klonet moneyplan-repoet, eller hvor du pakket ut zip-filen du lastet ned. 

# Konfigurasjon av Python

Sjekk at python er installert riktig ved å åpne et terminalvindu og skriv 'python'.  Hvis python sitt prompt vises som nedenfor er alt som det skal være.  Avslutt med 'exit()' og enter. 

For å holde styr på egen og andres programvare bruker Moneyplan et program som heter 'pipenv'.  Dette installeres med kommandoen  `pip install pipenv`.  
Hvis du er på en unix-variant må du gjerne prefixe denne kommandoen med 'sudo ', som i `sudo pip install pipenv`.

Deretter installleres bibliotekene som moneyplan bruker med kommandoen  `pipenv install` 

# Konfigurasjon av Moneyplan

Når du har skaffet deg nødvendige tilganger og lastet ned moneyplan, er det på tide å konfigurere Moneyplan.  I installasjonskatalogen for Moneyplan finner du filen config.json.  
Ta en kopi av denne til et sted utenfor katalogen du har installert moneyplan i, så unngår du at konfigurasjonen din blir overskrevet neste gang du laster ned Moneyplan. 

I  config.json må du sette opp hvilke kontoer du har i YNAB, og hvilke kontoer i SBanken disse kontoene skal synces med.  

```
{
  "antall_dager_bakover": 3,
  "flagg_farge" : "yellow",
  "cleared" : "uncleared"
  "kontoer_som_skal_synkroniseres": [
    {
      "kontonavn": "Kontonavn i YNAB",
      "kontonummer": "kontonummer i SBanken"
    },
    {
      "kontonavn": "Kontonavn2 i YNAB",
      "kontonummer": "kontonummer2 i SBanken"
    }
  ],
  "autentisering": {
    "sbanken": {
      "sb_userid": "ditt personnnummer",
      "sb_clientid": "...",
      "sb_secret": "...."
    },
    "ynab": {
      "ynab_api_key": "....",
      "ynab_budget_id": "...."
    }
  }
}
```

Her legger du inn så få/mange kontoer du vil. Pass på at det er et komma mellom hver kontodefinisjon.  

`antall_dager_bakover` angir hvor langt bakover i tid moneyplan skal hente transaksjoner.   Er tallet for lavt, kan det hende at det er transaksjoner du ikke får med deg.  
Er det for høyt får programmet undødvendig mye arbeid, men vil ikke dobbellaste transaksjoner. 

Moneyplan kan konfigureres med autentiseringsnøklene vi lastet ned tidligere på 2 måter:  Med environmentvariabler/miljøvariabler eller i config.json.
Environmentvariabler, hvis definert, har høyere prioritet enn config-filen. Variablene som brukes er: 

- SB_USERID
- SB_CLIENTID
- SB_SECRET
- YNAB_API_KEY 
- YNAB_BUDGET_ID 

Siste variabelen benyttes strengt tatt ikke til autentisering, men til å identifisere hvilket budsjett det moneyplan skal synkronisere transaksjoner inn i. 
Denne hentes fra adresse-linjen i nettleseren.  I eksempelet nedenfor kopierer du den delen som markeres i bildet. 

![](docs/images/budgetid.png)

Husk at hvis du gjør en _Fresh start_ i Ynab, vil det bli generert et nytt (kvasi-tomt) budsjett.  Dette vil ha en ny id, så da må verdien i SB_BUDGET_ID bli
oppdatert. 

## Kjøring av moneyplan

Siden jeg kjører moneyplan på min linux-server, har jeg laget meg følgende shell-script (run_moneyplan.sh):  

```
#!/bin/sh

cd /home/jantzen/moneyplan

export PATH=~/.local/bin:$PATH
export LANG=nb_NO.utf8
export SB_SECRET=...
export SB_CLIENTID=...
export YNAB_BUDGET_ID=...
# hentes fra ynab
export YNAB_API_KEY=...
#personnr
export SB_CUSTOMERID=...

pipenv run python main.py -c ../moneyplan.conf

```

Der hvor det står `...` har jeg satt inn mine verdier.   Da kjører jeg moneyplan med kommandoen `./run_moneyplan.sh` 

I tillegg kan moneyplan kjøres automatisk med linux-kommandoen crontab.  Nedenfor er min crontab-definisjon, som kjører moneyplan hver morgen kl 06.00. 

```
0 6 * * * /home/jantzen/run_moneyplan.sh > /home/jantzen/mp1.log 2>&1
```

### Kommandolinjeparametre

`moneyplan`kan kjøres med forskjellige kommandolinje-parametre: 

``` 
   -c eller --config <konfigurasjonsfil, f.eks config.json>
   -r eller --readonly  henter transaksjoner fra Sbanken, men skriver ikke inn i Ynab
   -d <n> eller --dager <n>  henter transaksjoner <n> dager tilbake i tid
   -D <dato> eller --dato <dato>  henter siden spesifikk dato, format yyyymmdd.
   -q eller --quiet, ikke skriv til skjermen
   -v eller --verbose logger debug info 
   
  ```